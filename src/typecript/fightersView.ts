import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { IFighter, IFighterDetail } from './helpers/dataInterfaces';
import { getFighterDetails } from './services/fightersService';

export function createFighters(fighters : IFighter[]): HTMLElement {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle, healFighter));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

export const fightersDetailsCache = new Map<string, IFighterDetail>();

async function healFighter(_: Event, fighter : IFighter): Promise<void> {
  if(fightersDetailsCache.has(fighter._id)){
    fightersDetailsCache.delete(fighter._id)
  }
}

async function showFighterDetails(_: Event, fighter : IFighter): Promise<void> {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string): Promise<IFighterDetail> {
  const cacheResult = fightersDetailsCache.get(fighterId);
  if(cacheResult)
  {
    return cacheResult;
  }
  const fighterDetail = await getFighterDetails(fighterId);
  fightersDetailsCache.set(fighterId, fighterDetail);
  return fighterDetail;
}

function createFightersSelector() {
  const selectedFighters = new Map<string, IFighterDetail>();

  return async function selectFighterForBattle(event: Event, fighter: IFighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((event.target as HTMLInputElement).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {

      const selectedFightersValues = selectedFighters.values();
      const winner = fight(selectedFightersValues.next().value, selectedFightersValues.next().value);
      showWinnerModal(winner);
    }
  }
}
