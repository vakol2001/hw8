import { CRITICAL_HIT, DODGE } from "./constants/fightChances";
import { IFighterDetail } from "./helpers/dataInterfaces";

export function fight(firstFighter: IFighterDetail, secondFighter: IFighterDetail): IFighterDetail {
  
  let [attacker, enemy] = Math.random() > 0.5 
    ? [firstFighter, secondFighter] 
    : [secondFighter, firstFighter];

  while(attacker.health > 0)
  {
    enemy.health -= getDamage(attacker, enemy);
    [enemy, attacker] = [attacker, enemy];
  }
  attacker.health = 0;
  return enemy;
}

export function getDamage(attacker: IFighterDetail, enemy: IFighterDetail) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter: IFighterDetail): number {
  return fighter.attack * getCriticalHitChance();
}

export function getBlockPower(fighter: IFighterDetail) {
  return fighter.defense * getDodgeChance();
}

function getCriticalHitChance() : number {
  return Math.random() * (CRITICAL_HIT.max - CRITICAL_HIT.min) + CRITICAL_HIT.min;
}

function getDodgeChance() : number {
  return Math.random() * (DODGE.max - DODGE.min) + DODGE.min;
}

