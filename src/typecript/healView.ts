import { fightersDetailsCache } from "./fightersView";
import { createElement } from "./helpers/domHelper";

export function createHealButton() : HTMLElement
{
    const button = createElement({tagName: 'button', className: 'heal-button'});
    button.innerText = 'Heal all';
    button.addEventListener('click',  _ => {
        fightersDetailsCache.clear();
    }, false);
    return button;
}