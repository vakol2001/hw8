import { IFighter } from './helpers/dataInterfaces';
import { createElement } from './helpers/domHelper'; 

type handleFunc = (event: Event, fighter : IFighter) => Promise<void>;
export function createFighter(fighter: IFighter, handleClick: handleFunc, selectFighter: handleFunc, handleHeal: handleFunc) : HTMLElement {
  const { name, source } = fighter;
  const nameElement = createName(name);
  const imageElement = createImage(source);
  const checkboxElement = createCheckbox();
  const fighterContainer = createElement({ tagName: 'div', className: 'fighter' });

  const healButton = createElement({tagName: 'button', className: 'fighter-heal-button'});
  healButton.innerText = `Heal ${fighter.name}`;
  
  fighterContainer.append(healButton,  imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev: Event) => ev.stopPropagation();
  const onCheckboxClick = (ev: Event) => selectFighter(ev, fighter);
  const onFighterClick = (ev: Event) => handleClick(ev, fighter);
  const onHealChick = (ev: Event) => {
    ev.stopPropagation();
    handleHeal(ev, fighter);
  }

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick , false);
  healButton.addEventListener('click', onHealChick, false);

  return fighterContainer;
}

function createName(name: string): HTMLElement {
  const nameElement = createElement({ tagName: 'span', className: 'name' });
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source: string): HTMLElement {
  const attributes = { src: source };
  return createElement({ tagName: 'img', className: 'fighter-image', attributes });
}

function createCheckbox(): HTMLElement {
  const label = createElement({ tagName: 'label', className: 'custom-checkbox' });
  const span = createElement({ tagName: 'span', className: 'checkmark' });
  const attributes = { type: 'checkbox' };
  const checkboxElement = createElement({ tagName: 'input', attributes });

  label.append(checkboxElement, span);
  return label;
}