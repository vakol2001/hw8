import { IFighter, IFighterDetail } from './dataInterfaces';
import { getFightersDetails, getFighters } from './mockData';
import { API_URL} from '../constants/apiURL';

const useMockAPI = true;

type ResponseModel = IFighter[] | IFighterDetail;

async function callApi(endpoint : string, method: string) : Promise<ResponseModel> {
  const url = API_URL + endpoint;
  const options = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response: Response) => (response.ok ? response.json() as Promise<{content: string}> : Promise.reject(Error('Failed to load'))))
        .then((result: {content: string}) => JSON.parse(result.content) as ResponseModel)
        .catch((error: Error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint : string) : Promise<ResponseModel>  {
  const response = endpoint === 'fighters.json' ? getFighters() : getFighterById(endpoint);

  return new Promise((resolve, reject : (error: Error) => void) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 1000);
  });
}

function getFighterById(endpoint : string) : IFighterDetail{
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);

  const result = getFightersDetails().find((it) => it._id === id);
  if(result === undefined)
  {
    throw new Error('Failed to load');
  }
  return result;
}

export { callApi };
