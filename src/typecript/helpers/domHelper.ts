interface ICreateElementOptions {
  tagName: string,
  className?: string,
  attributes?: { [index: string]: string }
}


export function createElement({ tagName, className, attributes = {} }: ICreateElementOptions) {
  const element = document.createElement(tagName);
  
  if (className) {
    className.split(' ').forEach(classItem => element.classList.add(classItem))
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
}