export interface IFighter {
    _id: string,
    name: string,
    source: string
}
  
export interface IFighterDetail extends IFighter  {
    health: number, 
    attack: number, 
    defense: number
}
  