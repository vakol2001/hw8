import { callApi } from '../helpers/apiHelper';
import { IFighter, IFighterDetail } from '../helpers/dataInterfaces';

export async function getFighters() : Promise<IFighter[]> {
  try {
    const endpoint = 'fighters.json';
    return await callApi(endpoint, 'GET') as IFighter[];

  } catch (error) {
    console.error(error)
    throw error;
  }
}

export async function getFighterDetails(id: string): Promise<IFighterDetail> {
    const endpoint = `details/fighter/${id}.json`;
    return await callApi(endpoint, 'GET') as IFighterDetail;
}

