import { IFighterDetail } from '../helpers/dataInterfaces';
import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export function showFighterDetailsModal(fighter: IFighterDetail): void{
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: IFighterDetail): HTMLElement {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });

  const imageWrapperElement = createElement({tagName: 'div', className: 'fighter-img-wrapper'});

  const imageElement = createElement({tagName: 'img', className: 'fighter-img', attributes: {src:source, alt: `${name}-image`}})
  imageWrapperElement.append(imageElement);

  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  nameElement.innerText = name;

  const table = createElement({tagName: 'table'});

  let attackString = '';
  for (let i = 0; i < attack; i++) {
    attackString+= '<i class="fas fa-fist-raised"></i> '
  }
  const attackRow = createRow('Attack', attack, attackString);

  let defenseString = '';
  for (let i = 0; i < defense; i++) {
    defenseString+= '<i class="fas fa-shield-alt"></i> '
  }
  const defenseRow = createRow('Defense', defense, defenseString);
  let healthString = '';
  if (health) {
    for (let i = 1; i < health / 10; i++) {
      healthString+= '<i class="fas fa-heart"></i> '
    }
    if (health % 10 !=0) {
      healthString+= '<i class="fas fa-heart-broken"></i> '
    } else {
      healthString+= '<i class="fas fa-heart"></i> '
    }
  } 
  const healthRow = createRow('Health', health, healthString);

  table.append(attackRow, defenseRow, healthRow);

  fighterDetails.append(imageWrapperElement,  nameElement, table);

  return fighterDetails;
}
function createRow(parametr: string, value: number, icons: string) : HTMLElement {
  const row = createElement({tagName: 'tr'});
  const parametrCell = createElement({tagName: 'td'})
  parametrCell.innerText = parametr;

  const valueCell = createElement({tagName: 'td'});
  valueCell.innerText = `${value}`;

  const iconsCell = createElement({tagName: 'td'});
  iconsCell.innerHTML = icons;

  row.append(parametrCell, valueCell, iconsCell);
  return row;

}

