import { IFighter } from "../helpers/dataInterfaces";
import { createElement } from "../helpers/domHelper";
import { showModal } from "./modal";

export  function showWinnerModal(fighter: IFighter) : void {
   
  const {name, source} = fighter;
  const bodyElement = createElement({tagName: 'div', className: 'modal-body'});
  
  const imageWrapperElement = createElement({tagName: 'div', className: 'fighter-img-wrapper'});

  const imageElement = createElement({tagName: 'img', className: 'fighter-img', attributes: {src:source, alt: `${name}-image`}})
  imageWrapperElement.append(imageElement);

  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  nameElement.innerText = name;
  bodyElement.append(imageWrapperElement, nameElement);
  showModal({title: 'Winner', bodyElement: bodyElement});
}    